var gBASE_URL = "https://pucci-mart.onrender.com/api";

var gLIST_PET_PER_PAGE = [];

var gPer_Page = 8 ;
let gTotalPages = 0; //total page number of products


$(document).ready(function () {
    onPageLoading();
})

function onPageLoading() {
    var vPage = 1;
    loadShopByPet(vPage);
    onGetPetsPaginationWithFilterClick();

}

function loadShopByPet(paramPageNumber) {
    const vQueryParams = new URLSearchParams({
        "_limit": gPer_Page,
        "_page": paramPageNumber - 1,
    });

    $.ajax({
        type: 'get',
        url: gBASE_URL + '/pets?' + vQueryParams.toString(),
        dataType: 'json',
        async: false,
        success: function (paramData) {
            gTotalPages = Math.ceil(paramData.count / gPer_Page);
            gLIST_PET_PER_PAGE = paramData;
            loadAllPetToList();
            handlePagination(paramPageNumber);
            console.log(paramData);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

// API lấy danh sách Pets phân trang có bộ lọc
function onGetPetsPaginationWithFilterClick() {
    const vQueryParams = new URLSearchParams();

    vQueryParams.append('_limit', 9);
    vQueryParams.append('_page', 0);
    vQueryParams.append('priceMin', 200);
    vQueryParams.append('priceMax', 300);
    vQueryParams.append('type', 'Cat');
    vQueryParams.append('type', 'Dog');

    $.ajax({
        type: 'get',
        url: gBASE_URL + '/pets?' + vQueryParams.toString(),
        dataType: 'json',
        async: false,
        success: function (paramData) {
            console.log(paramData);
        },
        error: function () {
            console.log(error);
        }
    });
}

function loadAllPetToList() {
    $(".all-pet__list").html("");
    for (var i in gLIST_PET_PER_PAGE.rows) {
        var vNowI = gLIST_PET_PER_PAGE.rows[i];
        $(".all-pet__list").append(`
        <div class="a-card-of-pet">
        <div class="a-card-of-pet__img-detail">
            <img src="${vNowI.imageUrl}">
        </div>
        <div class="a-card-of-pet__detail">
            <div class="a-card-of-pet__detail--name">${vNowI.name}</div>
            <div class="a-card-of-pet__detail--description">${vNowI.description}</div>
            <div class="a-card-of-pet__detail--price">
                <span class="a-card-of-pet__detail--description">$${vNowI.promotionPrice}</span>
                <s class="a-card-of-pet__detail--description">$${vNowI.price}</s>
            </div>
        </div>
    </div>
`)
    }
}


function handlePagination(){
    
}
